<div align="center">
    <img src="https://gitlab.com/grzgajda/find-longest-array/raw/master/logo.png" alt="find-longest-array" />
</div>

<div align="center">
    <strong>Remove items from the list in such a way that the rest items were sorted ascending and the list was as long as possible.</strong>
    <br />
    <br />
</div>

<div align="center">
  <img src="https://img.shields.io/badge/LANG-Go-%233960A9.svg?style=for-the-badge" />
  <img src="https://img.shields.io/gitlab/pipeline/grzgajda/find-longest-array.svg?logo=gitlab&style=for-the-badge" />
  <img src="https://img.shields.io/badge/coverage-93.8%25-brightgreen.svg?logo=gitlab&style=for-the-badge" />
</div>

<br />
<br />

It's one of the exercises I found on the Internet. Assume you have an array _(slice, unsorted list)_ with numbers in random order, e.g. `[]int{2, 4, 6, 4, 2}`. The idea behind this task is to find all numbers which makes the longest, sorted sequence.

## How to run

To run tests you need to install package _[stretchr/testify](https://github.com/stretchr/testify)_ in your Go environment (using command `go get`). After successful installation just run command:

```bash
go test . -cover
```

## Examples

```go
assertShort(t, dataProvider{
  example:  []int{2, 3, 1, 4},
  expected: []int{2, 3, 4},
})
```

```go
assertShort(t, dataProvider{
  example:  []int{2, 3, 1, 1, 6, 4, 2},
  expected: []int{2, 3, 6},
})
```

```go
// Expected output was [1, 3, 7, 10, 12, 16]
// but provided input has more solutions
// [1, 3, 7, 10, 12, 16]
// [1, 5, 7, 10, 12, 16]
// [1, 3, 7, 10, 14, 16]
// [1, 5, 7, 10, 14, 16]
assertShort(t, dataProvider{
  example:  []int{1, 9, 5, 13, 3, 11, 7, 15, 2, 10, 6, 14, 4, 12, 8, 16},
  expected: []int{1, 5, 7, 10, 12, 16},
})
```

## Solution

Package contains one exported function which takes as arguments an slice of numbers and returns slice of numbers. There is no any interaction with function, only tests (in _main\_test.go_).

```go
func Solve(nums []int) []int {}
```

I came with recursive solution where each time I try to find the lowest numbers in sequence from current position. Let's say we have slice `[]int{6, 5, 3, 4, 5, 6}`. Starting from position _0_, our lowest numbers are `[]int{5, 3}`. Number 5 is on position _1_, so there we have `[]int{3}` as the lowest number. So starting from position _0_, we need to move to position _1_ or _2_.