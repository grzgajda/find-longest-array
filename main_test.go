package main

import (
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

type dataProvider struct {
	example  []int
	expected []int
}

func assertShort(t *testing.T, d dataProvider) {
	assert.Equal(t, d.expected, Solve(d.example), "Input: %v", d.example)
}

func Test_Solution_SingleElement(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{1},
		expected: []int{1},
	})
}

func Test_Solution_FourElements(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{2, 3, 1, 4},
		expected: []int{2, 3, 4},
	})
}

func Test_Solution_SevenElements(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{2, 3, 1, 1, 6, 4, 2},
		expected: []int{2, 3, 6},
	})
}

func Test_Solution_SevenElements_LowestInMiddle(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{2, 3, 1, 2, 3, 4, 5},
		expected: []int{1, 2, 3, 4, 5},
	})
}

// Expected output was [1, 3, 7, 10, 12, 16]
// but provided input has more solutions
// [1, 3, 7, 10, 12, 16]
// [1, 5, 7, 10, 12, 16]
// [1, 3, 7, 10, 14, 16]
// [1, 5, 7, 10, 14, 16]
func Test_Solution_OriginalInput(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{1, 9, 5, 13, 3, 11, 7, 15, 2, 10, 6, 14, 4, 12, 8, 16},
		expected: []int{1, 5, 7, 10, 12, 16},
	})
}

func Test_Solution_OriginalInput_Shorten1(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{10, 6, 14, 4, 12, 8, 16},
		expected: []int{10, 12, 16},
	})
}

func Test_Solution_OriginalInput_Shorten2(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{2, 10, 6, 14, 4, 12, 8, 16},
		expected: []int{2, 10, 12, 16},
	})
}

func Test_Solution_OriginalInput_Shorten3(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{7, 15, 2, 10, 6, 14, 4, 12, 8, 16},
		expected: []int{7, 10, 12, 16},
	})
}

func Test_Solution_OriginalInput_Shorten4(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{3, 11, 7, 15, 2, 10, 6, 14, 4, 12, 8, 16},
		expected: []int{3, 7, 10, 12, 16},
	})
}

func Test_Solution_LongInput(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{3, 1, 4, 5, 10, 11, 6, 7, 8, 24, 32, 99, 33},
		expected: []int{3, 4, 5, 6, 7, 8, 24, 32, 99},
	})
}

func Test_Solution_LastNumberIsNot_Biggest(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{6, 7, 3, 4, 5, 1, 2},
		expected: []int{3, 4, 5},
	})
}

func Test_Solution_LastNumberIsNot_Biggest2(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{7, 3, 4, 5, 1, 2},
		expected: []int{3, 4, 5},
	})
}

func Test_Solution_LastSequence(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{7, 8, 9, 5, 6, 1, 2, 3, 4},
		expected: []int{1, 2, 3, 4},
	})
}

func Test_Solution_LastSequence_2(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{1, 6, 5, 7, 2, 3, 4},
		expected: []int{1, 2, 3, 4},
	})
}

func Test_Solution_TwoSequences(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{10, 11, 12, 1, 2, 3, 4},
		expected: []int{1, 2, 3, 4},
	})
}

func Test_Solution_ReverseSequenceInside(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{6, 5, 4, 3, 2, 1, 1, 2, 3, 4, 5, 6},
		expected: []int{1, 2, 3, 4, 5, 6},
	})
}

func Test_Solution_ArrayLength10000_Sorted(t *testing.T) {
	var data []int
	for i := 0; i < 10000; i++ {
		data = append(data, 10000-i)
	}

	assertShort(t, dataProvider{
		example:  data,
		expected: []int{1},
	})
}

func Test_Solution_ArrayLength10000_HalfSorted(t *testing.T) {
	var data []int
	var solution []int
	for i := 1; i < 5000; i++ {
		data = append(data, 5000-i)
	}
	for i := 1; i < 5000; i++ {
		data = append(data, i)
		solution = append(solution, i)
	}

	assertShort(t, dataProvider{
		example:  data,
		expected: solution,
	})
}

func Test_Solution_ArrayLength10000_SortedPlusTwo(t *testing.T) {
	var data []int
	var solution []int
	for i := 1; i < 5000; i = i + 2 {
		data = append(data, 5000-i)
	}
	for i := 1; i < 5000; i = i + 2 {
		data = append(data, i)
		solution = append(solution, i)
	}

	assertShort(t, dataProvider{
		example:  data,
		expected: solution,
	})
}

func Test_Solution_ArrayLength10000_WithMiddleOne(t *testing.T) {
	var data []int
	var solution []int

	for i := 0; i < 10000; i++ {
		if i == 7500 {
			data = append(data, 1)
		}
		if i != 7500 {
			data = append(data, i)
			solution = append(solution, i)
		}
	}

	assertShort(t, dataProvider{
		example:  data,
		expected: solution,
	})
}

func Test_Solution_BiggerNumbers(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{62, 45, 32, 45, 68, 99, 1, 2, 3, 4, 112, 111, 144},
		expected: []int{1, 2, 3, 4, 111, 144},
	})
}

func Test_Solution_LongSequence_BreakInside(t *testing.T) {
	assertShort(t, dataProvider{
		example:  []int{10, 11, 12, 13, 14, 15, 1, 2, 3, 4, 5, 25, 26, 27, 28, 29},
		expected: []int{10, 11, 12, 13, 14, 15, 25, 26, 27, 28, 29},
	})
}

func Benchmark_Length_OneHundred(b *testing.B) {
	var input []int
	for i := 1; i < 100; i++ {
		input = append(input, rand.Intn(i))
	}

	for n := 0; n < b.N; n++ {
		findLowest(input)
	}
}

func Benchmark_Length_OneThousand(b *testing.B) {
	var input []int
	for i := 1; i < 1000; i++ {
		input = append(input, rand.Intn(i))
	}

	for n := 0; n < b.N; n++ {
		findLowest(input)
	}
}

func Benchmark_Length_TenThousands(b *testing.B) {
	var input []int
	for i := 1; i < 10000; i++ {
		input = append(input, rand.Intn(i))
	}

	for n := 0; n < b.N; n++ {
		findLowest(input)
	}
}

func Benchmark_Length_OneHundredThousands(b *testing.B) {
	var input []int
	for i := 1; i < 100000; i++ {
		input = append(input, rand.Intn(i))
	}

	for n := 0; n < b.N; n++ {
		findLowest(input)
	}
}

func Benchmark_Length_OneMillion(b *testing.B) {
	var input []int
	for i := 1; i < 1000000; i++ {
		input = append(input, rand.Intn(i))
	}

	for n := 0; n < b.N; n++ {
		findLowest(input)
	}
}

func Benchmark_Length_OneMillion_Reversed(b *testing.B) {
	var data []int
	for i := 1; i < 5000; i++ {
		data = append(data, 5000-i)
	}
	for i := 1; i < 5000; i++ {
		data = append(data, i)
	}

	for n := 0; n < b.N; n++ {
		findLowest(data)
	}
}

func Benchmark_Length_OneMillion_ReversedPlusTwo(b *testing.B) {
	var data []int
	for i := 1; i < 5000; i = i + 2 {
		data = append(data, 5000-i)
	}
	for i := 1; i < 5000; i = i + 2 {
		data = append(data, i)
	}

	for n := 0; n < b.N; n++ {
		findLowest(data)
	}
}

func Test_IsSorted_TrueAscending(t *testing.T) {
	data := []int{1, 2, 3, 4, 5, 6, 7}
	_, ok := isSorted(data)

	assert.True(t, ok)
}

func Test_IsSorted_TrueDescending(t *testing.T) {
	data := []int{7, 6, 5, 4, 3, 2, 1}
	_, ok := isSorted(data)

	assert.True(t, ok)
}

func Test_IsSorted_False(t *testing.T) {
	data := []int{2, 3, 1, 1, 6, 4, 2}
	_, ok := isSorted(data)

	assert.False(t, ok)
}

func Test_RemoveReverseSorted_True(t *testing.T) {
	data := []int{6, 5, 4, 2, 2, 2}
	res := removeReverseSorted(data)

	assert.Equal(t, []int{2}, res)
}

func Test_RemoveReverseSorted_False(t *testing.T) {
	data := []int{2, 3, 1, 1, 6, 4, 2}
	res := removeReverseSorted(data)

	assert.Equal(t, data, res)
}

func Test_RemoveReverseSorted_Double(t *testing.T) {
	data := []int{10, 8, 6, 4, 2, 3, 5}
	res := removeReverseSorted(data)

	assert.Equal(t, []int{2, 3, 5}, res)
}

func Test_RemoveReverseSorted_NotPair(t *testing.T) {
	data := []int{7, 4, 5, 6, 7}
	res := removeReverseSorted(data)

	assert.Equal(t, data, res)
}

func Benchmark_RemoveReverseSorted(b *testing.B) {
	var data []int
	for i := 1; i < 5000; i++ {
		data = append(data, 5000-i)
	}
	for i := 1; i < 5000; i++ {
		data = append(data, i)
	}

	for n := 0; n < b.N; n++ {
		removeReverseSorted(data)
	}
}
