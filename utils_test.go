package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_FindMinMax(t *testing.T) {
	min, max := findMinMax([]int{9, 8, 7, 6, 5, 10, 6, 7, 8, 9})
	assert.Equal(t, 5, min)
	assert.Equal(t, 10, max)
}

func Test_SliceToMax(t *testing.T) {
	newSlice := sliceToMax([]int{1, 2, 3, 4, 5, 6}, 4)
	assert.Equal(t, []int{1, 2, 3, 4}, newSlice)
}

func Test_FindIndex(t *testing.T) {
	index := findFirstIndex([]int{1, 2, 3, 4, 5}, 3)
	assert.Equal(t, 2, index)
}

func Test_FindFirstIndex(t *testing.T) {
	index := findFirstIndex([]int{1, 2, 3, 4, 3, 5}, 3)
	assert.Equal(t, 2, index)
}

func Test_FindLowest(t *testing.T) {
	results := findLowest([]int{3, 4, 3, 6, 2})
	assert.Equal(t, []int{3, 2}, results)
}

func Test_FindLowest_Duplicates(t *testing.T) {
	results := findLowest([]int{1, 1, 6})
	assert.Equal(t, []int{1}, results)
}
