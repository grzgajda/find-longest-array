package main

import (
	"sort"
)

func main() {
	// do nothing
}

// Solve removes items from the list in such a way
// that the rest items were sorted ascending and
// the list was as long as possible.
func Solve(nums []int) []int {
	solution, _ := iterateSlice(nums, 0)

	return solution
}

func iterateSlice(nums []int, try int) ([]int, int) {
	maxDepth := try
	nums = removeReverseSorted(nums)
	if data, ok := isSorted(nums); ok == true {
		return data, maxDepth
	}

	var results []int
	for _, primary := range findLowest(nums) {
		var temporaryResults []int

		primaryIndex := findFirstIndex(nums, primary)
		temporarySlice := nums[primaryIndex+1:]

		_, max := findMinMax(temporarySlice)
		temporarySliceToMax := sliceToMax(temporarySlice, max)
		if len(temporarySliceToMax) > len(temporarySlice)/2 {
			temporarySlice = temporarySliceToMax
		}

		temporaryResults = addToResults(temporaryResults, primary)
		for i := 0; i < len(temporarySlice); i++ {
			if temporarySlice[i]-1 != temporaryResults[len(temporaryResults)-1] {
				break
			}
			temporaryResults = addToResults(temporaryResults, temporarySlice[i])
		}
		temporarySlice = temporarySlice[len(temporaryResults)-1:]

		depthData, depth := iterateSlice(temporarySlice, try+1)
		if depth > maxDepth {
			maxDepth = depth
		}
		for _, n := range depthData {
			temporaryResults = addToResults(temporaryResults, n)
		}

		if len(temporaryResults) > len(results) {
			results = temporaryResults
		}
	}

	return results, maxDepth
}

func removeReverseSorted(nums []int) []int {
	var results []int
	results = nums

	for i := range nums {
		if len(nums) > i+1 {
			if nums[i]-1 == nums[i+1] {
				results = nums[i+1:]
				continue
			}
			if nums[i] == nums[i+1] {
				results = nums[i+1:]
				continue
			}
			if nums[i] >= nums[i+1] && nums[i+1] >= nums[i+2] {
				results = nums[i+2:]
				continue
			}
		}

		return results
	}

	return results
}

func isSorted(nums []int) ([]int, bool) {
	if sort.IntsAreSorted(nums) {
		return nums, true
	}

	var reverse []int
	for i := len(nums) - 1; i > 0; i-- {
		reverse = append(reverse, nums[i])
	}

	if sort.IntsAreSorted(reverse) {
		return []int{reverse[0]}, true
	}

	return nums, false
}

func addToResults(num []int, n int) []int {
	if len(num) == 0 {
		num = append(num, n)
		return num
	}

	lastElement := num[len(num)-1]
	if lastElement < n && !inSlice(num, n) {
		num = append(num, n)
	}

	return num
}
