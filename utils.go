package main

func sliceToMax(nums []int, max int) []int {
	var maxKey int

	for key, val := range nums {
		if val == max {
			maxKey = key
		}
	}

	return nums[:maxKey+1]
}

func sliceFromMax(nums []int, max int) []int {
	var maxKey int

	for key, val := range nums {
		if val == max {
			maxKey = key
		}
	}

	return nums[maxKey:]
}

func findMinMax(nums []int) (int, int) {
	var minValue, maxValue int

	for i, v := range nums {
		if i == 0 {
			minValue = v
			maxValue = v
		}
		if v < minValue {
			minValue = v
		}
		if v > maxValue {
			maxValue = v
		}
	}

	return minValue, maxValue
}

func findFirstIndex(nums []int, val int) int {
	for k, v := range nums {
		if v == val {
			return k
		}
	}
	return 0
}

func findLowest(nums []int) []int {
	var theLowest []int
	for _, m := range nums {
		if len(theLowest) > 0 {
			lastLowestElement := theLowest[len(theLowest)-1]
			if lastLowestElement < m {
				continue
			}
		}
		if inSlice(theLowest, m) {
			continue
		}

		theLowest = append(theLowest, m)
	}

	return theLowest
}

func inSlice(nums []int, num int) bool {
	for _, n := range nums {
		if n == num {
			return true
		}
	}

	return false
}
